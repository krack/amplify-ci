FROM ubuntu:21.10
RUN apt update && apt install -y curl unzip

# install aws-cli v2
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
  && unzip awscliv2.zip \
  && ./aws/install

# install amplify-cli
RUN curl -sL https://aws-amplify.github.io/amplify-cli/install | bash && $SHELL

# Added by Amplify CLI binary installer
ENV PATH "$PATH:/root/.amplify/bin"
